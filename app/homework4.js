var vm = this;
var year = undefined;
var month = undefined;
var reportType = undefined;
var temperatureType = undefined;

function verifyInput() {
    var result = true;
    getUserInput();
    if ((vm.year != null || vm.year != undefined) &&
        (vm.month != null || vm.month != undefined) &&
        (vm.reportType != null || vm.reportType != undefined) &&
        (vm.temperatureType != null || vm.temperatureType != undefined)
        && isNumber(vm.year) && isNumber(month)) {

    	//execute input
    }else{
    	result = false;
    }
    return result;
}

function isNumber(number){
	if(isNan(number)){
		return false;
	}else{
		return true;
	}
}

function getUserInput() {
    vm.year = document.getElementById("year");
    vm.month = document.getElementById("month");
    var reportType = document.getElementById("listType");
    vm.reportType = reportType.options[reportType.selectedIndex].value;
    var temperatureType = document.getElementById("temperatureType");
    vm.temperatureType = temperatureType.options[temperatureType.selectedIndex].value;
}
